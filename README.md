# Rename-XaeroWorldMap-Folders

## The problem

The Minecraft mod [Xaero's World Map](https://www.curseforge.com/minecraft/mc-mods/xaeros-world-map)
does not work correctly on Minecraft servers that have periodically changing IP addresses,
especially self hosted servers.

This is because the mod stores the maps in directories with this format: `Multiplayer_127.0.0.1_DIM1`.\
As you can see, the IP address of the server is part of the name of the directories,
so when the IP address changes the mod creates new directories instead of using the old ones
(because of the different IP address).

## The solution

This shell script renames these directories by changing the IP address part in their names.
After that, the maps can be found/used by the mod again.

### Limitations:
- the script will only work with three (or less but not zero) directories containing an IP address
  in their name (one for each dimension) in the `XaeroWorldMap` directory
- the script has to be adjusted manually according to where the `XaeroWorldMap` directory can be found

## The Installation
1. Download the
  [rename-folders.sh](https://codeberg.org/FestplattenSchnitzel/Rename-XaeroWorldMap-Folders/src/branch/main/rename-folders.sh) and
  [World-Map-Fix.desktop](https://codeberg.org/FestplattenSchnitzel/Rename-XaeroWorldMap-Folders/src/branch/main/World-Map-Fix.desktop)
2. Move the `World-Map-Fix.desktop` on your desktop (/home/username/Desktop, /home/username/Schreibtisch or whatever)
3. Move the `rename-folders.sh` somewhere you like
4. Edit the `rename-folders.sh` (line 5), set the correct path to your (Minecraft) instance
5. Edit the `World-Map-Fix.desktop` (line 5), set the correct path to the shell script (see 3.)
6. Double-click the `World-Map-Fix.desktop` file and confirm, that you trust it
7. Enjoy! Feel free to ask if any questions/problems occur.
