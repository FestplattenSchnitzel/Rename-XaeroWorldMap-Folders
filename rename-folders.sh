#!/bin/bash

# CONFIG
# CAUTION: the path below must not have a "/" at the end
instance_path="/home/username/path/to/instance"

# read in the current ip address
echo "Please enter the current IP address:"
read ip_addr

# go to directory with map folders
cd "${instance_path}/XaeroWorldMap"

# find all matching folders
paths=($(find -maxdepth 1 -type d | egrep -o 'Multiplayer_[0-9]{2,3}\.([0-9]{1,3}\.){2}[0-9]{1,3}_.*$'))

# exit if the number of folders is not 3
if [ ${#paths[@]} -eq 0 ]; then
    echo "No directories to rename."
    exit 1
elif [ ${#paths[@]} -gt 3 ]; then
		echo "There are more than 3 folders with IP addresses in their names."
		echo "Please delete all folders except three ones with the same IP address."
		exit 1
fi

# substitute the old IP addresses in the folders names
for path in "${paths[@]}"
	do		
		old_addr=$(echo $path | egrep -o '[0-9]{2,3}\.([0-9]{1,3}\.){2}[0-9]{1,3}')
		new_path=$(echo $path | sed -e "s/${old_addr}/${ip_addr}/")
		
		mv $path $new_path
		
		echo "Renamed ${path} to ${new_path}."
done

printf "\n"
